# Introduction to Running and Deploying Our Code #
## User Guide ##
[How to access our platform](https://bitbucket.org/Kimbanu27/info3600_group2/wiki/Project%20Setup%20Guide%20/%20How%20To%20Access%20Our%20Platform)

## Self-hosting/Deployment guide ##
1. [Deploy a Server To Host The Platform](https://bitbucket.org/Kimbanu27/info3600_group2/wiki/Project%20Setup%20Guide%20/%20Deploy%20A%20Server%20To%20Host%20The%20Platform)
2. [Making Changes To The Platform](https://bitbucket.org/Kimbanu27/info3600_group2/wiki/Project%20Setup%20Guide%20/%20Making%20Changes%20To%20The%20Platform)
3. [Modifying The Meeting Page](https://bitbucket.org/Kimbanu27/info3600_group2/wiki/Project%20Setup%20Guide%20/%20Modifying%20The%20Meeting%20Page)
4. [Fixing The Platform When It Breaks](https://bitbucket.org/Kimbanu27/info3600_group2/wiki/What%20to%20do%20when%20the%20platform%20breaks)
